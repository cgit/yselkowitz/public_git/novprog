Name:           novprog
Version:        3.2.0
Release:        1%{?dist}
Summary:        Tool to graph your progress in writing a NaNoWriMo style novel

License:        GPLv3+
URL:            http://gottcode.org/%{name}/
Source:         http://gottcode.org/%{name}/%{name}-%{version}-src.tar.bz2

BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  qt5-qttools-devel

Requires:       hicolor-icon-theme

%description
NovProg is a tool to graph your progress in writing a NaNoWriMo style novel.
You enter your wordcount and it updates a graph showing you how much progress
you have made. It also shows you how far you are through your daily goal,
and your total goal. Mousing over a bar in the graph will show a tooltip
with that day’s wordcount.

%prep
%setup -q


%build
%{qmake_qt5} PREFIX=%{_prefix}
make %{?_smp_mflags}


%install
%make_install INSTALL_ROOT=%{buildroot}

%find_lang %{name} --with-qt --without-mo

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop || :
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/appdata/%{name}.appdata.xml || :

%files -f %{name}.lang
%doc ChangeLog CREDITS README
%license COPYING
%{_bindir}/%{name}
%{_datadir}/metainfo/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%dir %{_datadir}/%{name}/
%dir %{_datadir}/%{name}/translations/
%{_mandir}/man1/%{name}.1.*

%changelog
* Mon Nov 29 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 3.2.0-1
- new version

* Fri Jul 31 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 3.1.7-1
- new version

* Fri Jul 13 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.1-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_29_Mass_Rebuild

* Thu Feb 08 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.1-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Thu Jan 18 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 3.1.1-5
- Remove obsolete scriptlets

* Thu Aug 03 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Thu Jul 27 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Oct 11 2016 Mario Blättermann <mario.blaettermann@gmail.com> - 3.1.1-1
- New upstream version

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Sun Jan 31 2016 Mario Blättermann <mario.blaettermann@gmail.com> - 3.1.0-1
- New upstream version

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.0.0-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 3.0.0-3
- Rebuilt for GCC 5 C++11 ABI change

* Tue Feb 17 2015 Mario Blättermann <mario.blaettermann@gmail.com> - 3.0.0-2
- Add unowned directory to %%files

* Sun Feb 15 2015 Mario Blättermann <mario.blaettermann@gmail.com> - 3.0.0-1
- Initial package
